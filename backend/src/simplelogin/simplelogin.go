package simplelogin

import (
	"database/sql"
	"fmt"
	"log"
	"net/http"
	"simplelogin/usersvc"

	// MySql driver
	_ "github.com/go-sql-driver/mysql"

	"golang.org/x/oauth2"
	"golang.org/x/oauth2/google"
)

// SimpleLogin services and conf
type SimpleLogin struct {
	usvc   *usersvc.Service
	config map[string]string
	// usersvcDB
}

// New returns a Configured SimpleLogin
func New() *SimpleLogin {
	s := SimpleLogin{nil, map[string]string{}}

	s.Getenv()

	s.InitServices()

	return &s
}

// InitServices connects to DB and initialize a services (user this case)
func (s *SimpleLogin) InitServices() {
	log.Println("Connecting to database")
	// Fetch from ENV

	db, err := sql.Open("mysql", s.config["DB"])
	if err != nil {
		log.Fatal(err)
	}
	log.Println("Initializing database if needed")
	err = usersvc.InitDB(db)
	if err != nil {
		log.Fatal(err)
	}

	// Server side oauth
	oauthconf := &oauth2.Config{
		ClientID:     s.config["OAUTH_ID"],
		ClientSecret: s.config["OAUTH_SECRET"],
		RedirectURL:  "postmessage",
		Scopes: []string{
			"https://www.googleapis.com/auth/userinfo.email",
		},
		Endpoint: google.Endpoint,
	}

	s.usvc = &usersvc.Service{DB: db, OAuth: oauthconf}
}

// cross origin resource sharing allowance
func cors(next http.HandlerFunc) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Access-Control-Allow-Origin", "*") // Temporary here
		if r.Method == "OPTIONS" {
			w.Header().Set("Access-Control-Allow-Headers", "X-Tok")
			return
		}
		next(w, r)
	}
}

// A way to change all my middlewares
func myChain(next http.HandlerFunc) http.HandlerFunc {
	return LogHandler(cors(next))
}

// Serve start the server
func (s *SimpleLogin) Serve() {
	// Routes
	mux := http.NewServeMux()
	mux.HandleFunc("/user/register", myChain(s.registerHandler))
	mux.HandleFunc("/user/login", myChain(s.loginHandler))
	mux.HandleFunc("/user/profile", myChain(s.profileHandler))
	mux.HandleFunc("/user/logout", myChain(s.logoutHandler))
	mux.HandleFunc("/user/forgot", myChain(s.forgotPassHandler))
	mux.HandleFunc("/oauthCode", myChain(s.oauthCodeHandler))

	mux.HandleFunc("/", LogHandler(http.FileServer(http.Dir("./public")).ServeHTTP))
	// Try to serve static  files too

	port := s.config["PORT"]
	log.Println("Starting server at:", port)
	http.ListenAndServe(fmt.Sprintf(":%s", port), mux)

}

// StartServer creates a new SimpleLogin and serve
func StartServer() {
	s := New()
	s.Serve()
}
