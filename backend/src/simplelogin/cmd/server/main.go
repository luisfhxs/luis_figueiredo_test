package main

import (
	"log"
	"simplelogin"

	"dev.hexasoftware.com/hxs/prettylog"
)

func main() {

	log.SetFlags(0)
	log.SetOutput(prettylog.New())

	simplelogin.StartServer()

}
