package usersvc

import (
	"database/sql"
	"errors"

	"golang.org/x/oauth2"
)

var (
	//ErrAlreadyExists returns if data is duplicated
	ErrAlreadyExists = errors.New("Error already exists")
	ErrInvalidEmail  = errors.New("Invalid email")
	ErrNotFound      = errors.New("Entry not found")
	ErrUnauthorized  = errors.New("Not authorized to access resource")
)

// Service data for user service
type Service struct {
	// Necessary DB
	DB    *sql.DB
	OAuth *oauth2.Config
}

/*
	There could be Update and Update password where it would do a set pass only instead of update all user
	OR even variadic Update like:
	updateFields([]string{"Pass","Email"},"pass","newemail@mail.tld") - UPDATE User set Pass = ?, Email =?
	where it would build the Update SQL query internally since user of this package should not be aware of SQL
*/

// This functions should be moved to internal, but currently used by google handler

//Higher level: (The actual Service functions)

//Login logs user in by email and password
func (s *Service) Login(email, pass string) (*Token, error) {
	if pass == "" {
		return nil, errors.New("Empty password not allowed") // When user logs in with a google account the password would be empty, this way we prevent user to login with empty passwords
	}
	usr, err := s.findOneUserSQL("WHERE email = ? AND pass = ?", email, pass)
	if err != nil || usr == nil {
		return nil, err
	}

	return s.createToken(usr, TokenTypeLogin)
}

// I will register if not found
func (s *Service) LoginGoogleID(code string) (*Token, error) {
	guser, err := s.GetGAPIUser(code)
	if err != nil {
		return nil, err
	}

	var usr *User
	usr, err = s.findByGoogleID(guser.Sub)
	if err != nil {
		return nil, err
	}
	// If there is no user we create one
	if usr == nil {
		// First check if email exists
		usrEmail, err := s.findByEmail(guser.Email)
		if err != nil {
			return nil, err
		}
		if usrEmail != nil {
			//XXX: if user Exists We update googleID?? because we trust google email more?
			usrEmail.GoogleID = guser.Sub
			err = s.update(usrEmail)
			if err != nil {
				return nil, err
			}
			usr = usrEmail // Proceed with token
		} else { // No email or googleID found We create the user
			usr = &User{GoogleID: guser.Sub, Fullname: guser.GivenName + " " + guser.FamilyName, Email: guser.Email}
			err = s.create(usr)
			if err != nil {
				return nil, err
			}
		}
	}

	return s.createToken(usr, TokenTypeLogin)

}

//RegisterEmail user and returns email validation token
func (s *Service) RegisterEmail(email string, pass string) (*Token, error) {
	var err error
	usr := &User{Email: email, Pass: pass} // Hash pass

	err = s.create(usr) // Email confirm token
	if err != nil {
		return nil, err
	}
	tok, err := s.createToken(usr, TokenTypeLogin)
	if err != nil {
		return nil, err
	}
	return tok, nil
}

func (s *Service) GetProfile(tok string) (*User, error) {
	return s.findByToken(tok)
}

// UpdateProfile fields(Email,Fullname,Phone,Address,Pass) by the token and returns updated user
func (s *Service) UpdateProfile(tok string, fields map[string]string) (*User, error) {
	usr, err := s.findByToken(tok)
	if err != nil {
		return nil, err
	}
	if usr == nil {
		return nil, ErrUnauthorized
	}

	if v, ok := fields["Email"]; ok {
		if usr.Email != v { // Just verify if user trying to set an existing email while changing
			usrMail, err := s.findByEmail(v)
			if err != nil { // Different things but it will serve the purpose for now
				return nil, err
			}

			if usrMail != nil {
				return nil, ErrAlreadyExists
			}
		}
		usr.Email = v
	}
	if v, ok := fields["Fullname"]; ok {
		usr.Fullname = v
	}
	if v, ok := fields["Phone"]; ok {
		usr.Phone = v
	}
	if v, ok := fields["Address"]; ok {
		usr.Address = v
	}
	if v, ok := fields["Pass"]; ok && v != "" {
		usr.Pass = v // Hash this
	}

	err = s.update(usr)
	if err != nil {
		return nil, err
	}

	return usr, nil
}

//ForgotPass returns a password recovery token
func (s *Service) ForgotPass(email string) (*Token, error) {
	usr, err := s.findByEmail(email)
	if err != nil {
		return nil, err
	}
	if usr == nil {
		return nil, ErrNotFound
	}
	// Token should be consumed, delete and exchanged for real login token
	token, err := s.createToken(usr, TokenTypeResetPassword)
	if err != nil {
		return nil, err
	}

	return token, nil
}

//Logout removes user token
func (s *Service) Logout(tok string) error {
	return s.deleteToken(tok)
}
