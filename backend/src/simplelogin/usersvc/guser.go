package usersvc

import (
	"encoding/json"
	"log"

	"golang.org/x/oauth2"
)

//GUser google user
type GUser struct {
	Sub           string `json:"sub"`
	Name          string `json:"name"`
	GivenName     string `json:"given_name"`
	FamilyName    string `json:"family_name"`
	Profile       string `json:"profile"`
	Picture       string `json:"picture"`
	Email         string `json:"email"`
	EmailVerified string `json:"email_verified"`
	Gender        string `json:"gender"`
}

// GetGAPIUser get google api user by code
func (s *Service) GetGAPIUser(code string) (*GUser, error) {
	guser := GUser{}
	// Put this elsewhere in some func
	tok, err := s.OAuth.Exchange(oauth2.NoContext, code)
	if err != nil {
		log.Println("Err:", err)
		return nil, err
	}
	client := s.OAuth.Client(oauth2.NoContext, tok)
	guserres, err := client.Get("https://www.googleapis.com/oauth2/v3/userinfo")
	if err != nil {
		log.Println("Err:", err)
		return nil, err
	}
	defer guserres.Body.Close()
	json.NewDecoder(guserres.Body).Decode(&guser)

	return &guser, nil

}
