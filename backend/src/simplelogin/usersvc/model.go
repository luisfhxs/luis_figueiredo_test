package usersvc

import "time"

type TokenType string

var (
	TokenTypeLogin         = TokenType("login")
	TokenTypeResetPassword = TokenType("password")
)

// User user information
type User struct {
	ID       int64
	GoogleID string
	Fullname string
	Email    string
	Pass     string
	Phone    string
	Address  string
}

// Token for forgot password/login
type Token struct {
	ID      int64
	Token   string
	UserID  int64
	Type    TokenType
	Created time.Time
}
