package usersvc_test

import (
	"database/sql"
	"fmt"
	"os"
	"runtime"
	"strings"
	"testing"

	"simplelogin/usersvc"
	// Mysql driver
	_ "github.com/go-sql-driver/mysql"
)

var (
	testdbname = "simplelogin_test"
)

func TestInit(t *testing.T) {
	prepareDB(t)
	//destroyDB(t, db)
}

func TestRegister(t *testing.T) {
	us := getUserService(t)

	tok, err := us.RegisterEmail("test1@test.tld", "123456")
	failNotEq(t, err, nil)
	failEq(t, tok, nil)

	// TestDuplicate
	_, err = us.RegisterEmail("test1@test.tld", "xxx")
	failEq(t, err, nil)
}

func TestLogin(t *testing.T) {
	us := getUserService(t)

	_, err := us.RegisterEmail("test1@test.tld", "123456")
	failNotEq(t, err, nil)

	tok, err := us.Login("test1@test.tld", "123456")
	failNotEq(t, err, nil)

	failEq(t, tok, nil) // Must be equal

}
func TestLoginFail(t *testing.T) {
	us := getUserService(t)
	_, err := us.RegisterEmail("test1@test.tld", "123456")
	failNotEq(t, err, nil)

	tok, err := us.Login("test1@test.tld", "xxx")
	failNotEq(t, err, nil)

	failEq(t, tok, nil) // Must be equal nil
}

func TestTrim(t *testing.T) {
	us := getUserService(t)

	_, err := us.RegisterEmail("test@test.com     ", "1q2w3e    ")
	failNotEq(t, err, nil)

	tok, err := us.Login("test@test.com", "1q2w3e")
	failNotEq(t, err, nil)
	failEq(t, tok, nil)
}

func TestProfileSave(t *testing.T) {
	us := getUserService(t)
	tok, err := us.RegisterEmail("test1@test.tld", "123456")
	failNotEq(t, err, nil)

	var newName = "John doe"

	_, err = us.UpdateProfile(tok.Token, map[string]string{"Fullname": newName})
	failNotEq(t, err, nil)

	usr, err := us.GetProfile(tok.Token)
	failNotEq(t, err, nil)
	failEq(t, usr, nil)

	failNotEq(t, usr.Fullname, newName)
}

// Test Helpers
func prepareDB(t *testing.T) *sql.DB {

	var dburl = os.Getenv("DB")
	//t.Log("Preparing")
	if dburl == "" {
		t.Skip("Missing DB Environment variable, Database should be something like 'user:pass@/' test will reconstruct database per unit")
		return nil
	}

	// If no MYSQL ENV we skip
	db, err := sql.Open("mysql", fmt.Sprintf(dburl)) // New connection per test
	if err != nil {
		t.Fatal(err)
	}
	//t.Log("Droping database")
	_, err = db.Exec(fmt.Sprintf("DROP DATABASE IF EXISTS %s", testdbname))
	if err != nil {
		t.Fatal(err)
	}

	//t.Log("Creating")
	_, err = db.Exec(fmt.Sprintf("CREATE DATABASE IF NOT EXISTS %s", testdbname))
	if err != nil {
		t.Fatal(err)
	}

	db, err = sql.Open("mysql", fmt.Sprintf("%s%s", dburl, testdbname)) // Reconnect with db
	if err != nil {
		t.Fatal(err)
	}

	err = usersvc.InitDB(db)
	if err != nil {
		t.Fatal(err)
	}
	return db
}

func getUserService(t *testing.T) *usersvc.Service {
	db := prepareDB(t) // Create new
	return &usersvc.Service{DB: db, OAuth: nil}
}

func failNotEq(t *testing.T, v1 interface{}, v2 interface{}) {
	if v1 != v2 {
		t.Fatalf("%s - Not Equal %+v != %+v", line(), v1, v2)
	}
}
func failEq(t *testing.T, v1 interface{}, v2 interface{}) {
	if v1 == v2 {
		t.Fatalf("%s - Equal %+v == %+v", line(), v1, v2)
	}
}

// Helper to show where it failed
func line() string {
	_, file, line, _ := runtime.Caller(2)
	si := strings.LastIndex(file, "/") + 1
	return fmt.Sprintf("%s:%d", file[si:], line)
}
