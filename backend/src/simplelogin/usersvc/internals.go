package usersvc

import (
	"fmt"
	"simplelogin/utils"
	"time"
)

//User
func (s *Service) validate(u *User) error {
	if u.Email == "" { // or some regexp too
		return ErrInvalidEmail
	}

	// XXX: mail validations, phone, address etc
	return nil
}

// Helper function to fetch user from SQL
func (s *Service) findOneUserSQL(extra string, params ...interface{}) (*User, error) {
	var sel = "SELECT u.id, u.googleID, u.fullname, u.email, u.pass, u.phone, u.address FROM User u"
	res, err := s.DB.Query(fmt.Sprintf("%s %s", sel, extra), params...)
	if err != nil {
		return nil, err
	}
	defer res.Close()
	if !res.Next() { // Should return only one / first
		return nil, nil
	}

	var u User
	res.Scan(&u.ID, &u.GoogleID, &u.Fullname, &u.Email, &u.Pass, &u.Phone, &u.Address)

	return &u, nil
}

// FindByEmail fetch user by email or nil if not exists
func (s *Service) findByEmail(email string) (*User, error) {
	return s.findOneUserSQL("WHERE email =?", email)
}

// FindByGoogleID fetch user by googleId or nil if not exists
func (s *Service) findByGoogleID(googleID string) (*User, error) {
	return s.findOneUserSQL("WHERE googleID = ?", googleID)
}

// findByToken fetch user by token or nil if not exists
func (s *Service) findByToken(token string) (*User, error) {
	return s.findOneUserSQL("LEFT JOIN Token t ON u.ID = t.userID WHERE t.Token = ?", token)
}

// DeleteToken deletes token preventing user from logging in // AKA logout
func (s *Service) deleteToken(token string) error {
	_, err := s.DB.Exec("DELETE FROM Token WHERE Token.Token = ?", token)
	return err
}

// CreateToken creates token for user
func (s *Service) createToken(u *User, tokenType TokenType) (*Token, error) {
	rtok := utils.RandToken()
	tok := Token{0, rtok, u.ID, tokenType, time.Now().UTC()}
	// Can fail if already exists
	res, err := s.DB.Exec("INSERT INTO Token (token, userID, type, created) VALUES(?, ?, ?, ?)",
		tok.Token,
		tok.UserID,
		string(tok.Type),
		tok.Created)
	if err != nil {
		return nil, err
	}
	tok.ID, _ = res.LastInsertId()

	return &tok, nil
}

//Update overall user by u.ID
func (s *Service) update(u *User) error {
	err := s.validate(u)
	if err != nil {
		return err
	}
	_, err = s.DB.Exec("UPDATE User SET googleID = ?, email = ?, fullname = ?, pass = ?, phone = ?, address = ? WHERE Id = ?", u.GoogleID, u.Email, u.Fullname, u.Pass, u.Phone, u.Address, u.ID)
	if err != nil {
		return err
	}

	return nil
}

//Create a new user
func (s *Service) create(u *User) error {

	err := s.validate(u)
	if err != nil {
		return err
	}

	// Not exacly race condition safe unless I lock somewhere or a transaction
	// Unlikely but a User could create an email in between, either way the INSERT will fail if email exists
	qres, err := s.DB.Query("SELECT Id FROM User where Email = ?", u.Email)
	defer qres.Close()
	if qres.Next() { // Already exists
		return ErrAlreadyExists
	}
	res, err := s.DB.Exec("INSERT INTO User(googleID, fullname, email, pass, phone, address) VALUES (?,?,?,?,?,?)",
		u.GoogleID,
		u.Fullname,
		u.Email,
		u.Pass, // hash this
		u.Phone,
		u.Address,
	)

	if err != nil {
		return err
	}
	id, err := res.LastInsertId()
	if err != nil {
		return err
	}
	u.ID = id

	return nil
}
