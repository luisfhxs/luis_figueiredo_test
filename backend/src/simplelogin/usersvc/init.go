package usersvc

import (
	"database/sql"
)

var (
	dbname = "simplelogin"
)

// InitDB initialize tables, for simple demo purpose
func InitDB(db *sql.DB) error {
	var err error
	/*_, err = db.Exec(fmt.Sprintf(`CREATE DATABASE IF NOT EXISTS %s`, dbname))
	if err != nil {
		return err
	}
	_, err = db.Exec(fmt.Sprintf(`USE %s`, dbname))
	if err != nil {
		return err
	}*/

	_, err = db.Exec(`
	CREATE TABLE IF NOT EXISTS User(
		id integer NOT NULL AUTO_INCREMENT,
		googleId varchar(50) UNIQUE NOT NULL,
		fullname varchar(120) NOT NULL,
		email varchar(50) UNIQUE NOT NULL ,
		pass varchar(50),
		phone varchar(14),
		address varchar(255),
		PRIMARY KEY(id)
	)`)
	if err != nil {
		return err
	}

	_, err = db.Exec(`
	CREATE TABLE IF NOT EXISTS Token(
		id integer NOT NULL AUTO_INCREMENT,
		token varchar(64) NOT NULL,
		userID integer NOT NULL,
		type ENUM('login','password') NOT NULL,
		created datetime NOT NULL,
		PRIMARY KEY(id),
		FOREIGN KEY(userID) REFERENCES User(id)
	)`)

	// Add foreign key
	return err
}
