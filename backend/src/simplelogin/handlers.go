package simplelogin

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"net/url"
	"simplelogin/usersvc"
)

// Responses
func httpError(w http.ResponseWriter, code int, msg string) {
	w.WriteHeader(code)
	json.NewEncoder(w).Encode(map[string]string{"error": msg})
}

func httpJSON(w http.ResponseWriter, obj interface{}) {
	w.Header().Set("Content-type", "application/json")
	json.NewEncoder(w).Encode(obj)
}

// Handlers
func (s *SimpleLogin) registerHandler(w http.ResponseWriter, r *http.Request) {

	// Read body json
	regData := map[string]string{}
	defer r.Body.Close()
	err := json.NewDecoder(r.Body).Decode(&regData)
	if err != nil {
		httpError(w, http.StatusBadRequest, "Bad request")
		return
	}

	log.Println("Received register data:", regData)
	tok, err := s.usvc.RegisterEmail(regData["Email"], regData["Pass"])
	if err == usersvc.ErrAlreadyExists {
		httpError(w, http.StatusConflict, "Email")
		return
	}
	if err != nil {
		log.Println("Err:", err)
		httpError(w, http.StatusInternalServerError, "Error creating user")
		return
	}
	httpJSON(w, map[string]string{"token": tok.Token})
}

// Should send a token
func (s *SimpleLogin) loginHandler(w http.ResponseWriter, r *http.Request) {
	// Read body json
	dLogin := map[string]interface{}{}

	defer r.Body.Close()
	err := json.NewDecoder(r.Body).Decode(&dLogin)
	if err != nil {
		httpError(w, http.StatusBadRequest, "Bad request")
		return
	}
	log.Println("Received data:", dLogin)
	// We get a new token instead of a user
	utok, err := s.usvc.Login(dLogin["user"].(string), dLogin["pass"].(string))
	if err != nil {
		log.Println("Err:", err)
		httpError(w, http.StatusInternalServerError, "Generating token")
		return
	}

	if utok == nil {
		httpError(w, http.StatusUnauthorized, "Unauthorized")
		return
	}
	httpJSON(w, map[string]string{"token": utok.Token})
}

// Requires query param tok
func (s *SimpleLogin) profileHandler(w http.ResponseWriter, r *http.Request) {

	// Check authorization
	qtok := r.Header.Get("X-Tok")
	if qtok == "" {
		httpError(w, http.StatusUnauthorized, "Unauthorized")
		return
	}

	if r.Method == "GET" { // This could be better with a better muxer
		// GetProfileFunc
		user, err := s.usvc.GetProfile(qtok)
		if err != nil {
			log.Println(err)
			httpError(w, http.StatusInternalServerError, "Error fetching token")
			return
		}
		if user == nil {
			httpError(w, http.StatusUnauthorized, "Unauthorized")
			return
		}
		user.Pass = "" // do not send password
		log.Println("Sending user:", user)
		httpJSON(w, user)
		return
	}

	if r.Method == "POST" {
		// This part could be on service
		upData := map[string]string{}
		defer r.Body.Close()
		json.NewDecoder(r.Body).Decode(&upData)
		log.Println("Post", upData)

		user, err := s.usvc.UpdateProfile(qtok, upData)
		if err == usersvc.ErrAlreadyExists {
			httpError(w, http.StatusConflict, "Already exists")
			return
		}
		if err != nil {
			log.Println(err)
			httpError(w, http.StatusInternalServerError, "Error Updating")
			return
		}
		user.Pass = "" // do not send password
		httpJSON(w, user)
		return
	}

}

func (s *SimpleLogin) logoutHandler(w http.ResponseWriter, r *http.Request) {
	qtok := r.Header.Get("X-Tok")

	log.Println("Deleting token:", qtok)
	err := s.usvc.Logout(qtok)
	if err != nil {
		log.Println("Err:", err)
		httpError(w, http.StatusBadRequest, "Error clearing token")
	}
}

func (s *SimpleLogin) forgotPassHandler(w http.ResponseWriter, r *http.Request) {
	email := r.URL.Query().Get("email")
	cliurl := r.URL.Query().Get("url")

	// This logic could be in SVC
	tok, err := s.usvc.ForgotPass(email)
	if err == usersvc.ErrNotFound {
		httpError(w, http.StatusNotFound, "Not found")
		return
	}
	if err != nil {
		log.Println("Err:", err)
		httpError(w, http.StatusInternalServerError, "Error fetching token")
		return
	}

	log.Println("CliUrl:", cliurl)
	var reseturl = fmt.Sprintf("%s/?reset=%s", cliurl, url.QueryEscape(tok.Token))

	go s.SendRecoverEmail(email, reseturl) // async send email, error will be logged if any

	httpJSON(w, true)

}

// google Login
//XXX: This function should be improved
func (s *SimpleLogin) oauthCodeHandler(w http.ResponseWriter, r *http.Request) { // Login or new

	defer r.Body.Close()
	codeData := map[string]interface{}{}
	err := json.NewDecoder(r.Body).Decode(&codeData)
	if err != nil {
		httpError(w, http.StatusBadRequest, "Bad request")
		return
	}
	// HERE
	tok, err := s.usvc.LoginGoogleID(codeData["code"].(string))
	if err != nil {
		log.Println(err)
		httpError(w, http.StatusInternalServerError, "No login")
	}

	httpJSON(w, map[string]string{"token": tok.Token})
}
