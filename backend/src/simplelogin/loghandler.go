package simplelogin

import (
	"log"
	"net/http"
)

// LogHandler log middleware with responseCode awareness
type logHandler struct {
	http.ResponseWriter
	ResponseCode int
}

// WriteHeader save ResponseCode and pass trough
func (l *logHandler) WriteHeader(code int) {
	l.ResponseCode = code
	l.ResponseWriter.WriteHeader(code)
}

//LogHandler middleware
func LogHandler(next http.HandlerFunc) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		logh := &logHandler{w, 200}
		if next != nil {
			next(logh, r)
			log.Println(r.RemoteAddr, r.Method, "-", logh.ResponseCode, r.URL)
			return
		}
		log.Println(r.RemoteAddr, r.Method, "- 404", r.URL)
	}
}
