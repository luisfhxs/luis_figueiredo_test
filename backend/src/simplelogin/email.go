package simplelogin

import (
	"fmt"
	"log"
	"net/smtp"
)

// SendRecoverEmail it will send password recover email
func (s *SimpleLogin) SendRecoverEmail(email string, reseturl string) error {

	emailfrom := "app@loginsample.hexasoftware.com"
	header := make(map[string]string)
	header["From"] = emailfrom
	header["To"] = email
	header["Subject"] = "loginsample Password reset!"
	header["X-Mailjet-Campaign"] = "app"

	body := ""
	for k, v := range header {
		body += fmt.Sprintf("%s: %s\r\n", k, v)
	}
	body += "\r\n<Generic polite message>\nReset your password here:" + reseturl //XXX: We can load this from file and Execute text/template

	// Make a function out of this
	auth := smtp.PlainAuth(
		"",
		s.config["SMTP_USER"],
		s.config["SMTP_PASS"],
		s.config["SMTP"],
	)

	err := smtp.SendMail(
		fmt.Sprintf("%s:587", s.config["SMTP"]),
		auth,
		emailfrom,
		[]string{email},
		[]byte(body),
	)
	if err != nil {
		log.Println("EMAIL ERR:", err)
		return err
	}

	return nil
}
