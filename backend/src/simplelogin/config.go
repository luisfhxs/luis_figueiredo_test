package simplelogin

import "os"

// Defaults

var (
	defaults = map[string]string{
		"DB": "root@/simplelogin",

		"PORT": "3000",

		"OAUTH_ID":     "817746551534-b6gce2o9uiiciadk5c7bdfmt7ds82s24.apps.googleusercontent.com",
		"OAUTH_SECRET": "8D-yS1lQy9j8_WP0CVWnwbMq",

		"SMTP":      "in-v3.mailjet.com",
		"SMTP_USER": "88743dbf5589b6bef61a8445759a2d7e",
		"SMTP_PASS": "7fea674a78e4ed2b818329cfe9ea8a85",
	}
)

// Getenv loads config from ENV var or use defaults
func (s *SimpleLogin) Getenv() {
	for k := range defaults {
		if ev := os.Getenv(k); ev != "" {
			s.config[k] = ev
			continue
		}
		s.config[k] = defaults[k]
	}

}
