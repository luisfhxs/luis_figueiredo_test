
DOCKERREGISTRY=hexasoftware.com:5000

all:
	make -C backend
	make -C frontend

test:
	make -C backend test

clean:
	rm -rf DIST 

distclean: clean
	make -C backend clean


# Dev purposes
run: clean all
	cd DIST;PORT=8080 ./simplelogin


# docker Helpers
push: docker 
	docker push ${DOCKERREGISTRY}/loginsample

docker: all
	docker build -t ${DOCKERREGISTRY}/loginsample .

.PHONY: all clean docker
