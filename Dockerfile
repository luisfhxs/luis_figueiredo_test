FROM scratch
ADD docker/ca-certificates.crt /etc/ssl/certs/ca-certificates.crt
ADD DIST /goapp
WORKDIR /goapp
EXPOSE 3000
ENTRYPOINT ["./simplelogin"]


