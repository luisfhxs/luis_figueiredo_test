SimpleLogin
============


building:
```
#Inside project root
make
```

running:
```
# after building
cd DIST; ./simplelogin
```

docker:
```
docker build -t imagename .
```

docker run:
```
docker run -p 3000:3000 --name loginapp imagename
```

#### Environment variables:  

Name         | Purpose
-------------|-------------------
PORT         | start http server in this tcp PORT
SMTP         | smtp host
SMTP_USER    | user for smtp plainauth
SMTP_PASS    | password for smtp plainauth
OAUTH_ID     | oauth clientId
OAUTH_SECRET | Oauth secret



Backend
-------
I often try to make my packages go getable but in this situation the go part was written inside backend folder, 
the Makefile will set GOPATH as (GOPATH={projpath}/backend/deps:{projpaht}/backend) this way all 'go get' packages will be downloaded to backend/deps which makes it easy to gitignore and make clean  
   
Although this is completely separated from the frontend it will serve static content (the frontend)   
 
#### Token logic:  
* There are certain details in token logic like remember password token can be used to login instead of only using it to change password would not be hard to implement since I specified token type in data table
* When user requests a logout the token is deleted from server
* It is possible that tokens remain in server forever so it would need a schedule cleaning service (either third party or in app) or simple upon some user login
* Tokens(string) at client side are being stored in localStorage it would be possible to store in cookies since cookies can expire



 

Frontend
---------
Since frontend is not in analysis here, the code is a bit messy as I was copy pasting necessary functionalities from one place to another, it will have some repeated code.    
Depending on the situation I would have written a JS class/object to interact with REST server instead of placing $.ajax on button clicks all around,   
there are probably some issues like (did not clear a form after logout) or messages missing login successul, some messages can be watched in developer console as this is not a frontend test 
