
//var apiURL = "http://192.168.25.6:3000"
var apiURL = ""
var user;

$(function() {

	// 
	$('.message').html('').hide()
	$('.modal').on('show.bs.modal', function() {
		$('form .message').html('').hide() // Clear message on modal if any
		blurBg()
	}).on('hide.bs.modal', function() {
		blurBg(false)
	})

	// Page Start
	$('.screen.login').show(); // Default start screen

	
	var remToken = getParameterByName("reset")
	if (remToken != undefined) {
		localStorage["token"] = remToken;
		//confirm this token
	}


	// First check
	if( localStorage["token"] !== undefined) {
		// Verify token if OK go to profile
		// window.location="/profile.html"	
		var a = activity("Loading")
		$.ajax({
			type:'GET',
			url: apiURL +"/user/profile", 
			//data: {'tok':localStorage['token']},	
			dataType: 'json',
			beforeSend: function(xhr){xhr.setRequestHeader('X-Tok', localStorage['token']);},
			success: function(data) {
				user = data;
				updateUser(user)
				switchToScreen('profile')
				if(remToken != undefined) {
					$('#profilePassModal').modal('show')
				}

			}, 
			complete: function(e) {
				a.done()
			},
			error: function(e) {
				localStorage.removeItem('token')	
				commonError(e)
			}
		})
		// Check token with server
	}
	

	//////////////////////////////////////////////////////////////
	// Login
	////////////////////////////

	// Bind Events
	// LOGIN SCREEN
	$('.btn-signin-google').on('click',function(e) {
		e.preventDefault()
		var a = activity("Signing in with google")
		// Sometimes this is missing	
		auth2.grantOfflineAccess().then(function(data) {
			$.ajax({
				type: 'POST',
				url: apiURL + "/oauthCode",
				data: JSON.stringify(data),
				dataType: 'json',
				success: function(data) {
					user = undefined;
					// Store token in localstorage
					localStorage.setItem("token",data.token)
					switchToScreen("profile")

					//window.location = data["oauthURL"]
				},
				complete: function(e) {
					a.done()
				},
				error: commonError
			})

		},function(e) {
			a.done()
		})
		// Load url from server??
	})

	$('.form-signin').on('submit',function(e) {
		e.preventDefault()
		var $msg = $(this).find('.message')
		$msg.html('').hide()

		// Create a common frontend lib for this?
		var login ={
			user: $('#inputEmail').val(),
			pass: $('#inputPassword').val()
		}
		var a = activity("Signing in")
		$.ajax({
			type:'POST',
			url: apiURL +"/user/login", 
			data: JSON.stringify(login), 
			dataType:'json',
			statusCode: {
				'401':function() { $msg.html('Login invalid').show() }
			},
			success: function(data) {
				user = undefined
				localStorage.setItem("token",data.token)
				switchToScreen("profile")
				// Set something on localStorage like token or credential
			}, 
			complete: function(e) {
				a.done()
			},
			error: commonError
		})
	})

	$('.form-register').on('submit',function(e) {
		e.preventDefault()
		var $msg = $(this).find('.message')
		$msg.html('').hide()
		
		console.log('Registering.')
		var pass = $('#register-password').val()
		var passr = $('#register-passwordr').val()
		if( pass != passr ) {
			console.log('Passwords does not match')
			$msg.html('Passwords does not match').show()
			return
		}
		var nuser = {
			Email: $('#register-email').val(),
			Pass: pass
		}
		var a = activity("Registering");
		// Grab email and password
		$.ajax({
			type:'POST',
			url: apiURL + "/user/register",
			data: JSON.stringify(nuser),
			dataType: 'json',
			success: function(data) {
				user = undefined;
				localStorage.setItem("token",data.token)
				$('#registerModal').modal('hide').one('hidden.bs.modal',function() {
					console.log("Show profile now")
					$('#profileEditModal').modal('show')
				})
				// done , confirm email or something
			},
			statusCode: {
				'409': function() {
					$msg.html('Email already exists').show()
				}
			},
			complete: function() { a.done() } ,
			error: commonError
		})

	})
	// Needs form register
	$('.form-forgotpassword').on('submit',function(e) {
		e.preventDefault()
		var $msg = $(this).find('.message');
		$msg.html('').hide()
		var email = $('#forgot-email').val()

		var a = activity("Updating...")
		$.ajax({
				type:'GET',
				url: apiURL +"/user/forgot", 
				data: {'email':email,'url':window.location.href},
				dataType:'json',
				success: function(data) {
					$('#forgotPasswordModal').modal('hide')
					messageModal('password','You will receive an email on your email address with reset instructions')
				}, 
				statusCode: {
					'404':function() {
						$msg.html('Email not found').show()
					}
				},
				complete: function(e) {
					a.done()
				},
				error: function(e) {
					commonError(e)
					switchToScreen('login')
				}
			})
	})

	//////////////////////////////////////////////////////////////
	// Profile
	////////////////////////////


	$('.screen.profile').on('show',function() {
		if(localStorage['token'] === undefined ) {
			switchToScreen('login')
		}
		if( user ==undefined) {
			var a = activity("Loading profile")
			$.ajax({
				type:'GET',
				url: apiURL +"/user/profile", 
				//data: {'tok':localStorage['token']},
				dataType:'json',
				beforeSend: function(xhr){xhr.setRequestHeader('X-Tok', localStorage['token']);},
				success: function(data) {
					user = data;
					updateUser(user)
				}, 
				complete: function(e) {
					a.done()
				},
				error: function(e) {
					commonError(e)
					switchToScreen('login')
				}
			})
		}
	})

	$('#profileEditModal').on('shown.bs.modal',function() {
 		google.maps.event.trigger(map, "resize");
		// Fill stuff with user - reload profile why? (other page could have changed it)
		var a = activity("Preparing profile")
		$.ajax({
				type:'GET',
				url: apiURL +"/user/profile", 
				//data: {'tok':localStorage['token']},
				dataType:'json',
				beforeSend: function(xhr){xhr.setRequestHeader('X-Tok', localStorage['token']);},
				success: function(data) {
					user = data;
					updateUser(user)
					codeAddress(user.Address)
				}, 
				complete: function(e) {
					a.done()
				},
				error: function(e) {
					commonError(e)
					$('#profileEditModal').modal('hide')
					switchToScreen('login')
				}
			})
	})
	$('.form-profileedit').on('submit',function(e) {
		e.preventDefault();
		var $msg = $(this).find('.message')
		$msg.html('').hide()

		var a = activity("Updating profile")
		var updateObject = {}
		$('.form-profileedit [data-key]').each(function(index,val) {
			updateObject[$(val).attr('data-key')] = $(val).val()
		})
	
		$.ajax({
				type:'POST',
				url: apiURL +"/user/profile", //?tok=" + encodeURIComponent(localStorage['token']), 
				data: JSON.stringify(updateObject),
				dataType:'json',
				beforeSend: function(xhr){xhr.setRequestHeader('X-Tok', localStorage['token']);},
				success: function(data) {
					$('#profileEditModal').modal('hide')
					updateUser(data)
					messageModal("Profile","Saved successfully")
					$('.form-profileedit [data-key]').val('') // clearform
				}, 
				complete: function(e) {
					a.done()
				},
				statusCode: {
					'409': function() {
						$msg.html('Email already exists').show()
					}
				},
				error: function(e) {
					commonError(e)
				}
			})

		// Send to server to update By Token
	})


	$('#profilePassModal').on('show.bs.modal',function(e) {
		$('#profilePassModal .modal-title').html("Change password for: " + user.Email)
		$('.form-passedit [data-key]').val()
	})

	$('.form-passedit').on('submit',function(e) {
		e.preventDefault();
		var $msg = $(this).find('.message')
		$msg.html('').hide()

		var updateObject = {}
		$('.form-passedit [data-key]').each(function(index,val) {
			updateObject[$(val).attr('data-key')] = $(val).val()
		})
		if(updateObject.Pass != updateObject.Passr) {
			$msg.html('Passwords does not match')
			return
		}
	
		var a = activity("Updating password")
		$.ajax({
				type:'POST',
				url: apiURL +"/user/profile",
				data: JSON.stringify(updateObject),
				dataType:'json',
				beforeSend: function(xhr){xhr.setRequestHeader('X-Tok', localStorage['token']);},
				success: function(data) {
					if (remToken) {
						logout(); // Not so safe but well
						return;
					}
					$('#profilePassModal').modal('hide')
					updateUser(data)
					messageModal("Password","Password changed successfully")
				}, 
				complete: function(e) {
					a.done()
				},
				error: function(e) {
					commonError(e)
				}
			})

		// Send to server to update By Token

	})


	$('.btn-logout').on('click',function() {
		logout()
	})


	var tmr 
	$('#edit-address').on('input',function(e) {
		var $el  = $(this)
		if(tmr!==undefined)clearTimeout(tmr) // Reset timeout
		tmr = setTimeout(function() {
			codeAddress( $el.val())
		},800)
	})

})

function start() {

	gapi.load('auth2', function() {
		$('.btn-signin-google').removeAttr("disabled")
		auth2 = gapi.auth2.init({
			client_id: '817746551534-b6gce2o9uiiciadk5c7bdfmt7ds82s24.apps.googleusercontent.com',
			scope: 'https://www.googleapis.com/auth/userinfo.email'
		});
	});
}
var geocode
var map
var marker
function initMap() {
	geocoder = new google.maps.Geocoder();
	var latlng = new google.maps.LatLng(40.3537781,-105.2030254)
	var mapOptions = {
		zoom: 4,
		center: latlng
	}
	map = new google.maps.Map(document.getElementById('profile-map'), mapOptions);
	
	google.maps.event.addListener(map, 'click', function(event) {
		geocoder.geocode({
			'latLng': event.latLng
		}, function(results, status) {
			if (status == google.maps.GeocoderStatus.OK) {
				if (results[0]) {
					$('#profileEditModal [data-key=Address]').val(results[0].formatted_address);
					map.setCenter(results[0].geometry.location);
					if( marker != undefined) marker.setMap(null)
					marker = new google.maps.Marker({
						map: map,
						position: results[0].geometry.location
					});
					//alert(results[0].formatted_address);
				}
			}
		});
	});

}
function codeAddress(address) {
	if( marker != undefined) marker.setMap(null)

	if (address == "" ) { 
		if (navigator.geolocation) navigator.geolocation.getCurrentPosition(function(pos) {
			console.log("Ok location")
			 var me = new google.maps.LatLng(pos.coords.latitude, pos.coords.longitude);
			 map.setCenter(me);
		}, function(error) {
			console.log("Error browser location",error)
			var latlng = new google.maps.LatLng(40.3537781,-105.2030254)
			map.setCenter(latlng)
		})
		map.setZoom(10)
		return;
	}
	geocoder.geocode( { 'address': address}, function(results, status) {
		if (status == 'OK') {
			map.setCenter(results[0].geometry.location);
			map.setZoom(10)
			marker = new google.maps.Marker({
				map: map,
				position: results[0].geometry.location
			});
		} else {
			console.log('Geocode was not successful for the following reason: ' + status);
		}
	});
}
function switchToScreen(target) {
	console.log("Switching to screen: ", target)
	$('.screen:not(.' + target + ')').hide().trigger('hide');
	$('.screen.' + target).show().trigger('show')
}

function blurBg(val) {
	if (val === undefined) {
		$('.main').addClass('blur')
		return
	}
	$('.main').removeClass('blur')
}
function activity(msg) {
	blurBg()
	var $loverlay = $('.loader-overlay');
	$loverlay.find('.msg').html(msg)
	$loverlay.show()
	return {
		done: function() {
			blurBg(false)
			$loverlay.hide()
		}
	}
}
function commonError(e) {
	console.log("Err",e) // Pop message here e.statusCode()
}
function updateUser(user) {
	if (user === undefined)  {
		$('.screen.profile [data-key]').html('')
		return
	}

	for(var k in user) {
		$('.screen.profile [data-key=' + k + ']').html(user[k])
		$('#profileEditModal [data-key=' + k + ']').val(user[k])
	}
}

function logout() {
	var a = activity("Logging out")
	user = undefined;
	var tok = localStorage['token']
	localStorage.removeItem('token')
	//updateUser()
	$.ajax({
		type:'GET',
		url: apiURL +"/user/logout",  // Invalidate/delete token
		//data: {'tok':tok},
		beforeSend: function(xhr){xhr.setRequestHeader('X-Tok', tok);},
		success: function(data) {}, 
		complete: function(e) {
			location = "/"
		}, 
		error: commonError
	})

}


function getParameterByName(name, url) {
	if (!url) {
		url = window.location.href;
	}
	name = name.replace(/[\[\]]/g, "\\$&");
	var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
		results = regex.exec(url);
	if (!results) return null;
	if (!results[2]) return '';
	return decodeURIComponent(results[2].replace(/\+/g, " "));
}


function messageModal(title,msg) {
	$("#messageModal .modal-title").html(title)
	$("#messageModal .modal-body").html(msg)
	$("#messageModal").modal('show')
}

